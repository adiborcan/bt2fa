<?php


namespace app\Services;


use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

class EmailService
{

    public static function sendEmail($params){

        $mail = new PHPMailer(true);
        $response = [];
        try {

            $mail->SMTPDebug = SMTP::DEBUG_SERVER;
            $mail->isSMTP();
            $mail->Host       = $params['host'];
            $mail->SMTPAuth   = true;
            $mail->Username   = $params['username'];
            $mail->Password   = $params['password'];
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
            $mail->Port       = $params['port'];

            //Recipients
            $mail->setFrom($params['from'], $params['name']);
            $mail->addAddress($params['to']);     //Add a recipient

            //Content
            $mail->isHTML(true);                                  //Set email format to HTML
            $mail->Subject = $params['subject'];
            $mail->Body    = $params['body'];
            $mail->AltBody = $params['alt_body'];

            if($mail->send()){
                $response['success'] = "The email has already sent!";
            }

        } catch (\Exception $e) {
            $response['error'] = "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }

        return $response;

    }

}