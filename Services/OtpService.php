<?php


namespace app\Services;


use app\core\Application;
use app\models\Otp;
use app\models\User;
use DateInterval;
use DateTime;

class OtpService
{
    const INTERVAL = 120; //120 seconds code period expiration
    const SUBJECT = "OTP CODE";
    const NAME = "OTP APPLICATION";

    //private $currTime;
    private array $email;

    public function __construct()
    {

    }


    /**
     * @return mixed
     */
    public function getCurrTime():object
    {
        return new DateTime();
    }

    public function sendCode():bool
    {
        $user_id    = Application::$app->user->id;

        $data = [
            'user_id' => $user_id,
            'created_at' => $this->getCurrTime()->format('Y-m-d H:i:s'),
            'expire_at' => $this->getExpiredTime()->format('Y-m-d H:i:s'),
            'code' => $this->generateCode()
        ];

        $newOtp = new Otp();
        $newOtp->loadData($data);

        if ($newOtp->validate()) {
            Otp::delete(['user_id' => $user_id]);
            if ($newOtp->save()) {
                //send email
                $this->prepareEmail($data['code']);
                $response = EmailService::sendEmail($this->email);

                if(isset($response['success'])){
                    return true;
                }

                return false;
            }
        }
        return false;
    }

    private function prepareEmail($code){
        $this->email['to']          = Application::$app->user->email;
        $this->email['name']        = self::NAME;
        $this->email['host']        = Application::$app->emailConfig['host'];
        $this->email['port']        = Application::$app->emailConfig['port'];
        $this->email['username']    = Application::$app->emailConfig['username'];
        $this->email['password']    = Application::$app->emailConfig['password'];
        $this->email['from']        = Application::$app->emailConfig['from'];
        $this->email['subject']     = self::SUBJECT;

        //refactoring, email's templates should be define in file or templates

        $this->email['body']        = '<p>Code for identification:</p>'.$code;
        $this->email['alt_body']    = 'Code for identification: '.$code;
    }


    //set expired time
    private function getExpiredTime()
    {
        $currTime = $this->getCurrTime();
        $currTime->add(new DateInterval('PT' . (self::INTERVAL) . 'S'));
        return $currTime;
    }

    /**
     * @param int $length
     * @return string
     */
    function generateCode(int $length = 12):string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * set user status
     */
    public static function setUserStatus(){

        $user_id = Application::$app->user->id;
        $user = User::findOne(["id"=>$user_id]);
        if($user){
            $user->id = $user_id;
            $user->status = 1;
            $user->save();
        }
    }
}