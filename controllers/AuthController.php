<?php


namespace app\controllers;

use app\core\Application;
use app\core\Controller;
use app\core\midlewares\AuthMiddleWare;
use app\core\Request;
use app\core\Response;
use app\models\LoginForm;
use app\models\OtpForm;
use app\models\User;
use app\Services\OtpService;

class AuthController  extends  Controller
{

    public function __construct()
    {
        $this->registerMiddleware(new AuthMiddleWare(['otpverify']));
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return array|false|string|string[]|void
     */
    public function login(Request $request, Response $response)
    {
        $loginForm = new LoginForm();

        if($request->isPost()){
            $loginForm->loadData($request->getBody());
            if($loginForm->validate() && $loginForm->login()){
               if($this->sendOtp()){
                   $response->redirect('/otpverify');
               }else{
                   $response->redirect('/otperror');
               }
            }
        }

        return $this->render("login",['model'=>$loginForm]);
    }

    public function otpverify(Request $request, Response $response){

        $otpForm = new OtpForm();

        if($request->isPost()){
            $otpForm->loadData($request->getBody());
            if($otpForm->validate() && $otpForm->verifyCode()){
                OtpService::setUserStatus();
                Application::$app->session->setFlash('success','Verify code has been OK!');
                $response->redirect('/clientarea');
            }
       }

        $this->setLayout('auth');
        return $this->render("otpverify",['model'=>$otpForm]);
    }

    //error otp
    public function otperror(){
        $this->setLayout('auth');
        return $this->render("otperror");
    }

    /**
     * @param Request $request
     * @return array|false|string|string[]
     */
    public function register(Request $request)
    {
        $user = new User();

        if($request->isPost()){
            $user->loadData($request->getBody());

            if($user->validate() && $user->save()){
                Application::$app->session->setFlash('success','Thanks for registering!');
                Application::$app->response->redirect('/');
            }
        }

        //$this->setLayout('auth');
        return $this->render("register",['model'=>$user]);
    }

    //sent OTP(one time password) user
    private function sendOtp(){
        $otp = new OtpService();
        return $otp->sendCode();
    }

    /**
     * @param Response $response
     */
    public function logout(Request $request = null, Response $response){

        Application::$app->logout();
        $response->redirect('/');
    }
}