<?php

namespace app\controllers;

use app\core\Controller;

/**
 * Class SiteController
 * @package app\controllers
 */
class SiteController extends  Controller
{
    public function home(){

        $params = [
            'name'=>"Two factor Auth"
        ];

        return $this->render('home', $params);
    }

    public function clientarea(){

        return $this->render('clientarea');
    }

}