<?php

namespace app\core;

use app\core\db\Database;
use app\core\db\DbModel;
use Exception;

/**
 * Class Application
 */
class Application
{

    public static string $ROOT_DIR;
    public string $userClass;
    public Router $router;
    public Request $request;
    public Session $session;
    public Response $response;
    public Database $db;
    public static Application $app;
    public ?Controller $controller; //can be nullable
    public ?UserModel $user; //can be nullable
    public array $emailConfig;
    public View  $view;

    public function __construct($rootPath, array $config)
    {
        self::$ROOT_DIR = $rootPath;
        self::$app = $this;

        $this->userClass = $config['userClass'];
        $this->emailConfig = $config['email'];
        $this->request = new Request();
        $this->response = new Response();
        $this->session = new Session();
        $this->router = new Router($this->request, $this->response);
        $this->db = new Database($config['db']);
        $this->view = new View();

        $userId = Application::$app->session->get('user');

        if($userId){
            $primaryKey = $this->userClass::primaryKey();
            $this->user = $this->userClass::findOne([$primaryKey=>$userId]);
        }
    }

    /**
     * @return Controller
     */
    public function getController(): Controller
    {
        return $this->controller;
    }

    /**
     * @param Controller $controller
     */
    public function setController(Controller $controller): void
    {
        $this->controller = $controller;
    }

    public function run()
    {
        try {
            echo $this->router->resolve();
        }catch (Exception $e){
            echo "Access Forbiden!";
        }
    }

    /**
     * login method
     * @param DbModel $user
     */
    public function login(UserModel $user)
    {
        $this->user = $user;
        $primaryKey = $user->primaryKey();
        $primaryValue = $user->{$primaryKey};
        $this->session->set('user',$primaryValue);
        
        return true;
     }

     /*
      * logout method
      */
     public function logout(){
        $this->user = null;
        $this->session->remove('user');
     }

    public static function isGuest()
    {
        return !isset(self::$app->user);
    }
}