<?php


namespace app\core;

use app\core\midlewares\BaseMiddleware;

/**
 * Class Controller
 * @package app\core
 */
class Controller
{
    public string $layout = 'main';

    /** @var BaseMiddleware[] */

    protected array $middlewares = [];
    public string $action = '';

    public  function render($view,$params = []){
        return Application::$app->view->renderView($view,$params);
    }

    //set layout
    public function setLayout($layout){
        $this->layout = $layout;
    }

    public function registerMiddleware(BaseMiddleware $middleware){
        $this->middlewares[] = $middleware;
    }

    /**
     * @return BaseMiddleware[]
     */
    public function getMiddlewares(): array
    {
        return $this->middlewares;
    }

}