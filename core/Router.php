<?php

namespace app\core;

/**
 * Class Router
 */
class Router
{

    public Request $request;
    public Response $response;
    protected array $routes = [];


    /**
     * Router constructor.
     * @param Request $request
     * @param Response $response
     */
    public function __construct(Request $request, Response $response)
    {
        $this->request = $request;
        $this->response = $response;
    }

    /**
     * @param $path
     * @param $callback
     */
    public function get($path, $callback)
    {
        $this->routes['get'][$path] = $callback;
    }

    /**
     * @param $path
     * @param $callback
     */
    public function post($path, $callback)
    {
        $this->routes['post'][$path] = $callback;
    }

    public function resolve()
    {
        $path = $this->request->getPath();
        $method = $this->request->method();

        $callback = $this->routes[$method][$path] ?? false;

        if ($callback === false) {
            $this->response->setStatusCode(404);
            //return $this->renderView("_404");
            return Application::$app->view->renderView($callback);
        }

        if (is_string($callback)) {
            //return $this->renderView($callback);
            return Application::$app->view->renderView($callback);
        }

        if (is_array($callback)) {

            /**@var Controller $controller */
            $controller = new $callback[0]();
            Application::$app->controller = $controller;
            $controller->action = $callback[1];
            $callback[0] = $controller;

            foreach ($controller->getMiddlewares() as $middleware) {
                $middleware->execute();
            }

        }

        return call_user_func($callback, $this->request, $this->response);
    }

    /**
     * @param $view
     * @param array $params
     * @return array|false|string|string[]
     */
    public function renderView($view, $params = [])
    {
        return  Application::$app->router->renderView($view, $params);
    }

    public function renderContent($view, $params){
        return  Application::$app->view->renderView($view, $params);
    }


    protected function layoutContent()
    {
        $layout = Application::$app->controller->layout;

        ob_start();
        include_once Application::$ROOT_DIR . "/../views/layouts/$layout.php";
        return ob_get_clean();
    }

    /**
     * @param $view
     * @param array $params
     * @return false|string
     */
    protected function renderOnlyView($view, $params = [])
    {
        foreach ($params as $name => $value) {
            $$name = $value;
        }

        ob_start();
        include_once Application::$ROOT_DIR . "/../views/$view.php";
        return ob_get_clean();
    }

}