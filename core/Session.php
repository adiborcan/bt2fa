<?php

namespace app\core;

/**
 * Class Session
 * @package app\core
 */
class Session
{

    protected const FLASH_KEY = 'flash_messages';

    public function __construct()
    {
        session_start();
        $flashMessages = $_SESSION[self::FLASH_KEY] ?? [];
        foreach ($flashMessages as &$flashMessage){
            $flashMessage['remove'] = true;
        }

        $_SESSION[self::FLASH_KEY] = $flashMessages;

    }

    /**
     * @param $key
     * @param $message
     */
    public function setFlash($key, $message){
        $_SESSION[self::FLASH_KEY][$key] = [
            'remove' => false,
            'value'=>$message
        ];
    }

    /**
     * @param $key
     */
    public function getFlash($key){

        return  $_SESSION[self::FLASH_KEY][$key] ?? false;

    }

    public function __destruct()
    {
        $flashMessages = $_SESSION[self::FLASH_KEY] ?? [];
        foreach ($flashMessages as $key=>&$flashMessage){
            if($flashMessage['remove']){
                unset($flashMessages[$key]);
            }
        }

        $_SESSION[self::FLASH_KEY] = $flashMessages;
    }

    /**
     * @param $key
     * @param $val
     */
    public function set($key, $val){
        $_SESSION[$key] = $val;
    }

    /**
     * @param $key
     * @return false|mixed
     */
    public function get($key){
        return $_SESSION[$key] ?? false;
    }

    public function remove($key){
        unset($_SESSION[$key]);
    }

}