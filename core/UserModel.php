<?php


namespace app\core;

use app\core\db\DbModel;

abstract class UserModel extends DbModel
{
    public $id;

    abstract public function getDisplayName():string;
}