<?php


namespace app\core;


class View
{
    public  string $title = '';


    /**
     * @param $view
     * @param array $params
     * @return array|false|string|string[]
     */
    public function renderView($view, $params = [])
    {
        $viewContent = $this->renderOnlyView($view, $params);
        $layoutContent = $this->layoutContent();

        return str_replace('{{content}}', $viewContent, $layoutContent);
    }

    protected function layoutContent()
    {
        $layout = Application::$app->controller->layout;
        if(Application::$app->controller){
            $layout = Application::$app->controller->layout;
        }

        ob_start();
        include_once Application::$ROOT_DIR . "/../views/layouts/$layout.php";
        return ob_get_clean();
    }

    /**
     * @param $view
     * @param array $params
     * @return false|string
     */
    protected function renderOnlyView($view, $params = [])
    {
        foreach ($params as $name => $value) {
            $$name = $value;
        }

        ob_start();
        include_once Application::$ROOT_DIR . "/../views/$view.php";
        return ob_get_clean();
    }

}