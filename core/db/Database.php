<?php

namespace app\core\db;

use app\core\Application;
use app\core\helpers\Helper;
use PDO;

class Database
{
    public PDO $pdo;

    public function __construct(array $config){

        $dsn = $config['dsn'] ?? '';
        $user = $config['user'] ?? '';
        $password = $config['password'] ?? '';

        $this->pdo = new PDO($dsn,$user,$password);
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function applyMigrations(){
        $this->createMigrationsTable();
        $appliedMigrations = $this->getAppliedMigrations();


        $files = scandir(Application::$ROOT_DIR.'/migrations');
        $toAppliedMigratins = array_diff($files, $appliedMigrations);

        $newMigrations = [];
        if(sizeof($toAppliedMigratins) > 0) {
            foreach ($toAppliedMigratins as $migration) {
                if ($migration === '.' || $migration === '..') {
                    continue;
                }

                require_once Application::$ROOT_DIR . '/migrations/' . $migration;
                $className = pathinfo($migration, PATHINFO_FILENAME);

                $instance = new $className();
                Helper::log("Applying migration $migration");
                $instance->up();
                Helper::log("Applied migration $migration");
                $newMigrations[] = $migration;
            }

            if(!empty($newMigrations)){
                $this->saveMigrations($newMigrations);
            }else{
                Helper::log("All migrations are applied!");
            }
        }
    }

    public function createMigrationsTable(){

        //DROP TABLE IF EXISTS `migrations`;

        $sql = "
                CREATE TABLE IF NOT EXISTS `migrations`  (
                    `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
                    `migration` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
                    `created_at` timestamp NULL DEFAULT current_timestamp,
                    PRIMARY KEY (`id`) USING BTREE
                    ) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8";

        $this->pdo->exec($sql);
    }

    /**
     * run migrations from database
     * @return array
     */
    public function getAppliedMigrations(){
        $sql = "SELECT migration FROM migrations";
        $statement = $this->pdo->prepare($sql);
        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_COLUMN);
    }

    /**
     * @param array $migrations
     */
    private function saveMigrations(array $migrations){
        $strMigration = implode(",",array_map(fn($m)=>"('$m')",$migrations));
        $statement = $this->pdo->prepare("INSERT INTO `migrations` (migration) VALUES $strMigration ");
        $statement->execute();
    }

    /**
     * @param $sql
     */
    public function prepare($sql){
        return $this->pdo->prepare($sql);
    }
}