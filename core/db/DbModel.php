<?php


namespace app\core\db;

use app\core\Application;
use app\core\Model;

abstract class DbModel extends Model
{
    abstract public static function tableName(): string;
    abstract public function attributes(): array;
    abstract public static  function primaryKey(): string;

    //save model
    public function save(){
        $tableName  = $this->tableName();
        $attributes = $this->attributes();

        $params     = array_map(fn($attr)=>":$attr", $attributes);

        $statement  = self::prepare("INSERT INTO $tableName (".implode(',',$attributes).") VALUES (".(implode(',', $params)).")");

        foreach ($attributes as $attribute){
            $statement->bindValue(":$attribute", $this->{$attribute});
        }

        $statement->execute();
        return true;
    }

    /**
     * search one row with where criteria
     * @param $where
     * Ex:where [email=>adiborcan@gmail.com, 'firstname'=>Adrian]
     */
    public static function findOne($where){

        $tableNme = static::tableName();
        $attributes = array_keys($where);
        $sql = implode(" AND ",array_map(fn($attr)=>"$attr=:$attr",$attributes));
        $statement = self::prepare("SELECT * FROM $tableNme WHERE $sql");

        foreach ($where as $key => $item){
            $statement->bindValue(":$key", $item);
        }

        $statement->execute();
        return $statement->fetchObject(static::class);
    }

    /**
     * prepare SQL for execute
     * @param $sql
     * @return false|\PDOStatement
     */
    public static function prepare($sql){
        return Application::$app->db->pdo->prepare($sql);
    }

    public static function delete($where){

        $tableNme = static::tableName();
        $attributes = array_keys($where);
        $sql = implode(" AND ",array_map(fn($attr)=>"$attr=:$attr",$attributes));
        $statement = self::prepare("DELETE FROM $tableNme WHERE $sql");

        foreach ($where as $key => $item){
            $statement->bindValue(":$key", $item);
        }

        $statement->execute();
    }

}