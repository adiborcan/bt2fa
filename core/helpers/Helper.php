<?php


namespace app\core\helpers;

class Helper
{
    /**
     * @param $var
     * @param false $exit
     */
    public  static function debug($var,bool $exit = false){
        echo '<pre>';
        print_r($var);
        echo '</pre>';

        if($exit){
            exit();
        }
    }

    /**
     * display log
     * @param $message
     */
    public static function log($message){
        echo '['.date('Y-m-d H:i:s').' ] - '.$message.PHP_EOL;
    }

}