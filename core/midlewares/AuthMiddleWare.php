<?php

namespace app\core\midlewares;

use app\core\Application;
use app\core\exception\ForbiddenException;

class AuthMiddleWare extends  BaseMiddleware
{

    /**
     * AuthMiddleWare constructor.
     * @param array $actions
     */
    public function __construct(array $actions =[])
    {
        $this->actions = $actions;
    }

    public function execute()
    {
        if(Application::isGuest()){
            if(empty($this->actions) || in_array(Application::$app->controller->action,$this->actions)){
                throw new ForbiddenException();
            }
        }
    }
}