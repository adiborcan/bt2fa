<?php


namespace app\core\midlewares;


abstract class BaseMiddleware
{
    abstract public function execute();
}