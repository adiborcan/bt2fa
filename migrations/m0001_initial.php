<?php

class m0001_initial{
    public function up(){
        $db = \app\core\Application::$app->db;
        $sql = "DROP TABLE IF EXISTS `users`;
                CREATE TABLE `users`  (
                    `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
                    `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
                    `firstname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
                    `lastname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
                    `status` tinyint(1) NOT NULL,
                    `created_at` timestamp NULL DEFAULT current_timestamp,
                PRIMARY KEY (`id`) USING BTREE
                ) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8;";
        $db->pdo->exec($sql);
    }

    public function down(){
        $db = \app\core\Application::$app->db;
        $sql = "DROP TABLE IF EXISTS `users`;";
        $db->pdo->exec($sql);

    }

}