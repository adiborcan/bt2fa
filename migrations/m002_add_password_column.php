<?php

class m002_add_password_column
{
    public function up(){

        $db = \app\core\Application::$app->db;
        $sql = "ALTER TABLE users ADD column password VARCHAR(512) NOT NULL;";
        $db->pdo->exec($sql);

    }

    public function down(){

        $db = \app\core\Application::$app->db;
        $sql = "ALTER TABLE users DROP column password;";
        $db->pdo->exec($sql);

    }
}