<?php

class m003_create_otp_table
{
    public function up(){

        $db = \app\core\Application::$app->db;
        $sql = "CREATE TABLE `otp`  (
                `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
                `user_id` int UNSIGNED NULL DEFAULT NULL,
                `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
                `created_at` timestamp NOT NULL,
                `expire_at` timestamp NULL DEFAULT NULL,
                PRIMARY KEY (`id`) USING BTREE,
                INDEX `idx_otp_user_id`(`user_id`) USING BTREE,
                CONSTRAINT `fk_otp_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
                ) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8;";
        $db->pdo->exec($sql);

    }

    public function down(){

        $db = \app\core\Application::$app->db;
        $sql = "DROP TABLE IF EXISTS `otp`;";
        $db->pdo->exec($sql);
    }
}