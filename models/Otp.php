<?php


namespace app\models;


use app\core\db\DbModel;
use DateTime;

class Otp extends DbModel
{
    const  SETINTERVAL = 120;
    public  int $user_id            = 0;
    public  string  $code           = '';
    public  string  $created_at     = '';
    public  string  $expire_at     = '';


    public static function tableName(): string
    {
        return 'otp';
    }

    public function attributes(): array
    {
        return ['user_id','code','created_at','expire_at'];
    }

    public static function primaryKey(): string
    {
        return  'id';
    }

    public function rules(): array
    {
        return [
            'user_id'  => [self::RULE_REQUIRED],
            'code'  =>  [self::RULE_REQUIRED, [self::RULE_UNIQUE,'class'=>self::class]],
            'created_at'  => [self::RULE_REQUIRED],
            'expire_at'  => [self::RULE_REQUIRED]
        ];
    }

    public function isValidCode(){

        $curtime = new DateTime();
        $expire_at = new DateTime($this->expire_at);
        $interval = $curtime->getTimestamp() - $expire_at->getTimestamp();
        if(intval($interval) > self::SETINTERVAL){
            return false;
        }

        return true;
    }

}