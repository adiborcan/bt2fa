<?php


namespace app\models;

use app\core\Application;
use app\core\Model;


class OtpForm extends Model
{
    public string $code = '';

    public function rules(): array
    {
        return [
            'code' => [self::RULE_REQUIRED]
        ];
    }

    //login
    public function verifyCode()
    {
        $user_id = Application::$app->user->id;

        $code = Otp::findOne(['code' => $this->code, 'user_id' => $user_id]);
        if (!$code) {
            $this->addError('code', 'Your code is invalid!');
            return false;
        }

        if ($this->code != $code->code) {
            $this->addError('code','Your code is invalid');
            return false;
        }

        //verify expiration time
        if(!$code->isValidCode()){
            $this->addError('code','Your code is expired!');
            return false;
        }

        return  true;
    }

    //labels
    public function labels(): array
    {
        return [
            'code' => 'Code'
        ];
    }
}