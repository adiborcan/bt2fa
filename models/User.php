<?php


namespace app\models;

use app\core\UserModel;

class User extends UserModel
{
    CONST STATUS_INACTIVE   = 0;
    CONST STATUS_ACTIVE     = 1;
    CONST STATUS_DELETED    = 2;

    public  string $firstname       = '';
    public  string $lastname        = '';
    public  string $email           = '';
    public  int   $status           = self::STATUS_INACTIVE;
    public  string $password        = '';
    public  string $passwordConfirm = '';

    public static function tableName(): string
    {
        return 'users';
    }

    public function attributes(): array
    {
        return ['firstname','lastname','email','password','status'];
    }

    /**
     * @return bool
     */
    public function save(){

        $this->password = password_hash($this->password,PASSWORD_DEFAULT);
        $this->status   = self::STATUS_INACTIVE;

        return parent::save();
    }

    /**
     * @return array[]
     */
    public function rules(): array
    {
        return [
                'firstname'  => [self::RULE_REQUIRED],
                'lastname'  => [self::RULE_REQUIRED],
                'email'     => [self::RULE_REQUIRED, self::RULE_EMAIL, [self::RULE_UNIQUE,'class'=>self::class]],
                'password'     => [self::RULE_REQUIRED,[self::RULE_MIN, 'min'=>8],[self::RULE_MAX,'max'=>12]],
                'passwordConfirm'     => [self::RULE_REQUIRED,[self::RULE_MATCH,'match'=>'password']],
        ];
    }

    public function labels()
    {
        return [
            'firstname'=>'First name',
            'lastname'=>'Last name',
            'email'=>'E-mail',
            'password'=>'Password',
            'passwordConfirm'=>'Confirm password',
        ];
    }

    public static function primaryKey(): string
    {
        return  'id';
    }

    //get Display Name
    public function getDisplayName():string{
        return  $this->firstname.' '.$this->lastname;
    }

}