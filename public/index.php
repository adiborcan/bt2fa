<?php

use app\core\Application;
use app\controllers\SiteController;
use app\controllers\AuthController;
use app\models\User;

require_once __DIR__ . '/../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__));
$dotenv->load();

date_default_timezone_set("Europe/Bucharest");

$config = [
    'userClass'=> User::class,
    'db' => [
        'dsn'=>$_ENV['DB_DSN'],
        'user'=>$_ENV['DB_USER'],
        'password'=> $_ENV['DB_PASSWORD']
    ],
    'email'=>[
        'host'=>$_ENV['EMAIL_HOST'],
        'username'=>$_ENV['EMAIL_USERNAME'],
        'password'=>$_ENV['EMAIL_PASSWORD'],
        'from'=>$_ENV['EMAIL_FROM'],
        'port'=>$_ENV['EMAIL_PORT']
    ]
];

$app = new Application(dirname(__FILE__), $config);

$app->router->get('/',[SiteController::class,'home']);
$app->router->get('/contact',[SiteController::class,'contact']);
$app->router->post('/contact',[SiteController::class,'contact']);


$app->router->get('/login',[AuthController::class,'login']);
$app->router->post('/login',[AuthController::class,'login']);

$app->router->get('/register',[AuthController::class,'register']);
$app->router->post('/register',[AuthController::class,'register']);

$app->router->get('/logout',[AuthController::class,'logout']);


//OTP ZONE
$app->router->get('/otpverify',[AuthController::class,'otpverify']);
$app->router->post('/otpverify',[AuthController::class,'otpverify']);
$app->router->get('/otperror',[AuthController::class,'otperror']);
$app->router->get('/outarea',[AuthController::class,'outarea']);

$app->router->get('/resend',[AuthController::class,'resend']);

$app->router->get('/clientarea',[SiteController::class,'clientarea']);

$app->run();