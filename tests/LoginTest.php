<?php
declare(strict_types=1);

use app\core\Application;
use app\models\LoginForm;
use PHPUnit\Framework\TestCase;

class LoginTest extends  TestCase
{
    public function testAuthentification():void{

        new Application();

        $credential = [
            'email'=>'adiborcan@gmail.com',
            'password'=>'ZETAcwn2020#'
        ];

        $loginForm = new LoginForm();
        $loginForm->loadData($credential);

        $this->assertEquals(true,$loginForm->validate() && $loginForm->login());

    }
}