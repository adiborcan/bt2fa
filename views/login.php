<?php
/** @var $model \app\models\User */
/** @var $this \app\core\View */

$this->title = "Login";
?>

<h1>Login form</h1>

<?php use app\core\form\Form;
$model = isset($model) ? $model : null;
$form = Form::begin('','post'); ?>

<?php echo $form->field($model,'email'); ?>
<?php echo $form->field($model,'password')->passwordField(); ?>
<br>
<button type="submit" class="btn btn-primary">Login</button>

<?php Form::end(); ?>