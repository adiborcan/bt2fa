<?php
/** @var $model \app\models\OtpForm */
/** @var $this \app\core\View */

$this->title = "OTP Verify";
?>

<h1>Verify</h1>

<?php use app\core\form\Form;
$model = isset($model) ? $model : null;
$form = Form::begin('','post'); ?>
<?php echo $form->field($model,'code'); ?>
<br>
<div class="row">
    <div class="col-1">
        <button type="submit" class="btn btn-primary">Verify</button>
    </div>
    <div class="col-1">
        <button type="button" class="btn btn-primary" onclick="alert('Resend Code!')" >Resend</button>
    </div>
</div>


<?php Form::end(); ?>

